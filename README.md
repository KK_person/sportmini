## 功能介绍 
    
运动场馆预约小程序是一款主要针对城市运动预约的工具类程序， 产品主要服务人群为20-45岁运动爱好者，程序前后端完整代码，包括场馆动态，运动常识，羽毛球场地预约，足球场地预约，篮球场地预约，健身房预约，乒乓球场地预约，游泳场地预约 ，网球场地预约等，可以按照业者的实际情况进行取舍或者增添场馆类型，既适用于大型综合性文体中心，也适合单一性运动场馆（如羽毛球馆，健身房，乒乓球馆等）。 采用腾讯提供的小程序云开发解决方案，无须服务器和域名。

- 场馆预约管理：开始/截止时间/人数均可灵活设置，可以自定义客户预约填写的数据项
- 场馆预约凭证：支持线下到场后校验签到/核销/二维码自助签到等多种方式
- 详尽的场馆预约数据：支持预约名单数据导出Excel，打印

 ![输入图片说明](demo/%E4%BA%8C%E7%BB%B4%E7%A0%81.png)

## 技术运用
- 本项目使用微信小程序平台进行开发。
- 使用腾讯专门的小程序云开发技术，云资源包含云函数，数据库，带宽，存储空间，定时器等，资源配额价格低廉，无需域名和服务器即可搭建。
- 小程序本身的即用即走，适合小工具的使用场景，也适合快速开发迭代。
- 云开发技术采用腾讯内部链路，没有被黑客攻击的风险，安全性高且免维护。
- 资源承载力可根据业务发展需要随时弹性扩展。  



## 作者
- 如有疑问，欢迎骚扰联系我鸭：开发交流，技术分享，问题答疑，功能建议收集，版本更新通知，安装部署协助，小程序开发定制等。
- 俺的微信:

![输入图片说明](https://gitee.com/naive2021/smartcollege/raw/master/demo/author.jpg)



## 演示
 ![输入图片说明](demo/%E4%BA%8C%E7%BB%B4%E7%A0%81.png)
 

 

## 安装

- 安装手册见源码包里的word文档




## 截图
![输入图片说明](demo/%E9%A6%96%E9%A1%B5.png)
![输入图片说明](demo/%E7%BE%BD%E6%AF%9B%E7%90%83.png)
 ![输入图片说明](demo/%E8%B6%B3%E7%90%83.png)
![输入图片说明](demo/%E7%AF%AE%E7%90%83.png)
![输入图片说明](demo/%E7%AF%AE%E7%90%83.png) 
![输入图片说明](demo/%E5%81%A5%E8%BA%AB%E6%88%BF.png)
![输入图片说明](demo/%E6%97%A5%E5%8E%86.png)
![输入图片说明](demo/%E9%A2%84%E7%BA%A6%E6%8A%A5%E5%90%8D.png)
![输入图片说明](demo/%E9%A2%84%E7%BA%A6%E8%AF%A6%E6%83%85.png)  
![输入图片说明](demo/%E6%88%91%E7%9A%84.png)
![输入图片说明](demo/%E5%B8%B8%E8%AF%86.png)

![输入图片说明](demo/%E5%8A%A8%E6%80%81.png)
 ![输入图片说明](demo/%E9%A2%84%E7%BA%A6%E7%A0%81.png)


## 后台管理系统截图
![输入图片说明](demo/%E5%90%8E%E5%8F%B0-=%E9%A2%84%E7%BA%A6%E7%AE%A1%E7%90%86.png)
![输入图片说明](demo/%E5%90%8E%E5%8F%B0-%E8%8F%9C%E5%8D%95.png)

![输入图片说明](demo/%E5%90%8E%E5%8F%B0-%E6%9F%A5%E7%9C%8B%E5%90%8D%E5%8D%95.png)
![输入图片说明](demo/%E5%90%8E%E5%8F%B0-%E5%AF%BC%E5%87%BA.png) 
![输入图片说明](demo/%E5%90%8E%E5%8F%B0-%E5%90%8D%E5%8D%95.png)
![输入图片说明](demo/%E5%90%8E%E5%8F%B0-%E8%87%AA%E5%8A%A9%E7%AD%BE%E5%88%B0.png) 

![输入图片说明](demo/%E5%90%8E%E5%8F%B0-%E6%97%B6%E9%97%B4%E8%AE%BE%E7%BD%AE.png)
![输入图片说明](demo/%E5%90%8E%E5%8F%B0-%E6%B4%BB%E5%8A%A8%E6%B7%BB%E5%8A%A0.png)
